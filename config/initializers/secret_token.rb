# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Treebook::Application.config.secret_key_base = 'c4ef201ac6c011c3db00a1552fab1d90111d61f529771ff1583092e3c459513c6dab8a10ffbb8acdd0f78016730dbd0160b8dc2ae37c0cc3b5fc3e3a1ac9336f'
